<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', [ 'as' => 'login', 'uses' => 'loginController@index']);
Route::post('/','loginController@verifyUser');
Route::get('dashboard','dashboard@index')->middleware('auth');  
Route::get('addnew','manageuser@index')->middleware('auth');
Route::post('addnew','manageuser@store')->middleware('auth');
Route::get('usermanagement/edit/{id}','manageuser@editusers')->middleware('auth');
Route::post('usermanagement/edit/{id}','manageuser@updateuser')->middleware('auth');
Route::get('usermanagement/delete/{id}','manageuser@deleteUser')->middleware('auth');
Route::get('logout', 'loginController@logout');
