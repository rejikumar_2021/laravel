<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
        @if (count($errors) > 0)
        <div class = "alert alert-danger">
           <ul>
              @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
              @endforeach
           </ul>
        </div>
     @endif
    <form action="" method="post">
            {{ csrf_field() }}
            <input type="text" name="username" id="" placeholder="username"><br>
            <input type="email" name="useremail" id="" placeholder="your email"><br>
            <input type="password" name="password" placeholder="Your password"><br>
            <input type="submit" value="Submit" name="submit">
    </form>
</body>
</html>