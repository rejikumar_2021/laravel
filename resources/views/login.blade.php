<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>login</title>
</head>
<body>
        @if (count($errors) > 0)
        <div class = "alert alert-danger">
           <ul>
              @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
              @endforeach
           </ul>
        </div>
     @endif
     
    <form action="" method="POST">
            {{ csrf_field() }}
    username <input type="text" name="username" autocomplete="off" value="{{old('username')}}"><br>
        password <input type="password" name="userpassword" id=""><br>
        <input type="submit" value="Login">
    </form>
</body>
</html>