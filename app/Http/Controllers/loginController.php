<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
/*use Illuminate\Support\Facades\Hash;*/
use Auth;
class loginController extends Controller
{
    public function index()
    {
        //$password = Hash::make('admin');
       // echo $password;
       return view('login');
    }
    public function verifyUser(Request $request)
    {
        $this->validate($request,[
            'username'=>'required',
            'userpassword'=>'required'
        ],[
            'username.required' => 'Please enter your name',
            'userpassword.required'=>'Please enter your password'
        ]);
        $username=$request->username;
        $password=$request->userpassword;

        if (Auth::attempt(['email' => $username, 'password' => $password])){
            return redirect()->intended('dashboard');
        }else{
            return view('login')->with('error','Invalid username or password');
        }
        
    }
    public function logout() {
        Auth::logout();
        return redirect('/');
      }
}
