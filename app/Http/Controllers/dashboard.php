<?php

namespace App\Http\Controllers;
use App\userModel;
class dashboard extends Controller
{
    public function index()
    {
        $data=userModel::orderBy('id','desc')->get();
        return view('dashboard')->with('users',$data);
    }
}
