<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\userModel;
class manageuser extends Controller
{
    public function index()
    {
        return view('addUser');
    }

    public function store(request $request)
    {
       
        $this->validate($request,[
            'username'=>'required',
            'password'=>'required',
            'useremail'=>'required|email'
        ],[
            'username.required' => 'Please enter your name',
            'password.required'=>'Please enter your password',
            'useremail.required'=>'Please enter your email'
        ]);
        $password=$request->input('password');
        $encripted= Hash::make($password);
        $data=new userModel;
        $data->name=$request->input('username');
        $data->email=$request->input('useremail');
        $data->password=$encripted;
        $data->save();
        return redirect('dashboard');
       
    }
    public function editusers($id)
    {
        $data=userModel::find($id);
       return view('profile')->with('user',$data);
    }

    public function updateuser(request $request,$id)
    {
        $this->validate($request,[
            'username'=>'required',
            'email'=>'required|email'
        ],[
            'username.required' => 'Please enter your name',
            'email.required'=>'Please enter your email'
        ]);
        $data=userModel::find($id);
        $data->name=$request->input('username');
        $data->email=$request->input('email');
        $data->save();
        return redirect('dashboard')->with('success','Data updated successfully');
    }

    public function deleteUser($id)
    {
        $data=userModel::find($id);
        $data->delete();
        return redirect('dashboard')->with('success','Data deleted successfully');
    }
}
